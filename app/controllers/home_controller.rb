class HomeController < ApplicationController
  def index
  end

  def help
  end

  def about
  end

  def calendar
    @date = params[:date] ? Date.parse(params[:date]) : Date.today
  end
end
