module ApplicationHelper

  def full_title(title)
    base_title = "Thought Tribe"
    if title.empty?
      base_title
    else
      "#{base_title}|#{title}"
    end
  end

  def current_class(link)
    current_page?(link) ? "active" : ""
  end
end
