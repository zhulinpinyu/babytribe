require 'spec_helper'

describe "Homes" do
  
  subject { page } 
  
  describe "Index page" do
    before { visit root_path }
    it { should have_title(full_title('')) }
    it { should_not have_title("|Home") }
  end

  describe "Help page" do
    before { visit help_path }
    it { should have_content('Help') }
    it { should have_title(full_title('Help')) }  
  end

  describe "About" do
    before { visit about_path }
    it { should have_content('About') }
    it { should have_title(full_title('About'))}
  end
end
